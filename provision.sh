#!/usr/bin/env bash

# elgws/provision.sh
#
# given a centos (5|6|7) installation, install lots of stuff

_pwd=$(pwd)

set -euo pipefail && \
    env | sort && \
    yum -y install deltarpm epel-release && \
    yum -y update && \
    yum -y upgrade && \
    yum -y groupinstall "Development Tools" && \
    yum -y install \
        GeoIP-devel \
        cgdb \
        check-devel \
        coreutils \
        curl \
        curl-devel \
        file-devel \
        g++ \
        gcc \
        git \
        gperf \
        inotify-tools \
        java-1.7.0-openjdk \
        libcurl-devel \
        libicu-devel \
        libopendkim-devel \
        libopendmarc-devel \
        openldap-devel \
        openssl-devel \
        python \
        python-devel \
        python-libs \
        python-tools \
        python26 \
        python26-devel \
        python26-libs \
        python26-tools \
        re2-devel \
        tar \
        which \
        xz \
        xz-devel \
    && \
    yum -y clean all && \
    >stamp.txt date -uIs


# gnu tar
cd ${_pwd} && \
    curl -sLO https://ftp.gnu.org/gnu/tar/tar-1.29.tar.xz && \
    tar -xJf tar-1.29.tar.xz && \
    cd tar-1.29 && \
    FORCE_UNSAFE_CONFIGURE=1 ./configure && \
    make clean all install && rm -rf $(pwd)


cd ${_pwd} && \
    curl -sLO https://ftp.gnu.org/gnu/autoconf/autoconf-2.69.tar.gz && \
    tar -xzf autoconf-2.69.tar.gz && \
    cd autoconf-2.69 && \
    ./configure && \
    make install
cd ${_pwd} && \
    curl -sLO https://ftp.gnu.org/gnu/automake/automake-1.15.tar.gz && \
    tar -xzf automake-1.15.tar.gz && \
    cd automake-1.15 && \
    ./configure && \
    make install
cd ${_pwd} && \
    curl -sLO https://ftp.gnu.org/gnu/libtool/libtool-2.4.6.tar.gz && \
    tar -xzf libtool-2.4.6.tar.gz && \
    cd libtool-2.4.6 && \
    ./configure && \
    make install
cd ${_pwd} && \
    curl -sLO https://ftp.gnu.org/gnu/autoconf-archive/autoconf-archive-2016.03.20.tar.xz && \
    tar -xJf autoconf-archive-2016.03.20.tar.xz && \
    cd autoconf-archive-2016.03.20 && \
    ./configure && \
    make install


# put mongo-c-driver and (by transitivity) libbson in special places
cd ${_pwd} && \
    curl -sLO https://github.com/mongodb/mongo-c-driver/releases/download/1.1.10/mongo-c-driver-1.1.10.tar.gz && \
    tar -xzf mongo-c-driver-1.1.10.tar.gz && \
    cd mongo-c-driver-1.1.10 && \
    ./configure --prefix=/usr --libdir=/usr/lib64 && \
    make install


# libspf2
cd ${_pwd} && \
    curl -sLO http://www.libspf2.org/spf/libspf2-1.2.10.tar.gz && \
    tar -xzf libspf2-1.2.10.tar.gz && \
    cd libspf2-1.2.10 && \
    ./configure --prefix=/usr --libdir=/usr/lib64 && \
    make install


# libarchive
cd ${_pwd} && \
    curl -sLO http://www.libarchive.org/downloads/libarchive-3.1.2.tar.gz && \
    tar -xzf libarchive-3.1.2.tar.gz && \
    cd libarchive-3.1.2 && \
    ./configure --prefix=/usr --libdir=/usr/lib64 && \
    make install


# libcheck
cd ${_pwd} && \
    curl -sLO https://github.com/libcheck/check/files/71408/check-0.10.0.tar.gz && \
    tar -xzf check-0.10.0.tar.gz && \
    cd check-0.10.0 && \
    ./configure --prefix=/usr --libdir=/usr/lib64 && \
    make install


# jq
cd ${_pwd} && \
    curl -sLO https://github.com/stedolan/jq/releases/download/jq-1.5/jq-1.5.tar.gz && \
    tar -xzf jq-1.5.tar.gz && \
    cd jq-1.5 && \
    ./configure --prefix=/usr --disable-maintainer-mode && \
    make install


# pip
cd ${_pwd} && \
    set -eu && \
    set -o pipefail && \
    test -x /usr/bin/python26 && \
    ln -sf /usr/bin/python26 /usr/local/bin/python || echo "no python26"
python -V
curl -sL https://bootstrap.pypa.io/get-pip.py | python && \
    pip install -U argparse && \
    pip list


cd ${_pwd}
