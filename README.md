# elgws #

[![Docker Pulls](https://img.shields.io/docker/pulls/nroza/elgws.svg)](https://hub.docker.com/r/nroza/elgws/)

### What is this? ###

* provisioning script and docker images for development

### How do I get set up? ###

```
$ docker pull nroza/elgws
```

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review

### Who do I talk to? ###

* Neil Roza <nroza@silversky.com>