# elgws/el5.dockerfile

# provision all the stuff we need

from centos:5
maintainer Neil Roza <nroza@silversky.com>
copy provision.sh /workdir/provision.sh
run cd /workdir && ./provision.sh
run rm -rf /workdir
